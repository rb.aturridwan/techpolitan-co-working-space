(function($) {
  'use strict';
  if ($("#fileuploader").length) {
    $("#fileuploader").uploadFile({
      url: "/upload-payment-proof",
      fileName: "payment-proof",
      uploadStr: "Upload Payment Proof",
      formData: {
        booking_id: $("#booking_id").val()
      },
      maxFileCount: 1,
      allowedTypes: "pdf, jpg, jpeg, png",
      onSuccess: function(files, data, xhr) {
        if (data.status == "success") {
          $("#payment-proof-uploaded").show();
          $("#payment-proof-uploaded").html(data.message);
          $("#payment-proof-uploaded").delay(5000).fadeOut();
          // redirect to payment page after 5 seconds
          setTimeout(function() {
            window.location.href = "/payments";
          }, 1000);
        } else {
          $("#payment-proof-uploaded").show();
          $("#payment-proof-uploaded").html(data.message);
          $("#payment-proof-uploaded").delay(5000).fadeOut();
        }
      }
    });
  }
})(jQuery);