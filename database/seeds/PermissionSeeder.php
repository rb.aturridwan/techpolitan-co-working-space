<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Facades\Schema;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $adminPermissions = Permission::adminDefaultPermissions();

        foreach ($adminPermissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms]);
        }

        // create roles and assign existing permissions
        $admin = Role::create(['name' => 'administrator']);
        $customer = Role::create(['name' => 'customer']);

        $admin->givePermissionTo(Permission::all());
        $customer->givePermissionTo('view_profile');
        $customer->givePermissionTo('edit_profile');
        $customer->givePermissionTo('change_password');
        

        Schema::enableForeignKeyConstraints();

    }
}
