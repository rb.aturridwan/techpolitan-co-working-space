<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Schema;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        $user = User::create([
            'name' => 'Admin Techpolitan',
            'email' => 'admin@techpolitan.co',
            'password' => Hash::make("password"),
            'email_verified_at' => now(),
        ]);

        $user->assignRole('administrator');

        // $user->assignRole('administrator');

        Schema::enableForeignKeyConstraints();

    }
}
