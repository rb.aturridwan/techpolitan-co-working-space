@extends('layouts.app')

@section('content')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">

                <ol>
                    <li><a href="index.html">Home</a></li>
                    <li>Search for Daily Pass</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="portfolio" class="inner-page portfolio">
        <div class="container" data-aos="fade-up">

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 contact d-flex mb-5">
                    <form action="{{url('search-coworking')}}" class="php-email-form" action="GET">
                        @csrf
                        <div class="align-items-end contact d-flex">
                            <!-- form search schedule -->
                            <div class="form-group" style="
                                margin-right: 1.5rem;
                            ">
                                <label for="">Date</label>
                                <input type="date" class="form-control" id="date" name="date" placeholder="Search Schedule" value="{{$date}}">
                            </div>
                            <div class="form-group" style="
                                margin-right: 1.5rem;
                            ">
                                <label for="">Pax</label>
                                <input type="number" class="form-control" id="pax" name="pax" placeholder="Pax" value="{{$pax}}">
                            </div>

                            <div class="form-group">
                                <button type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="section-title">
                    <h2>Search Result for {{Carbon\Carbon::parse($date)->format('d/m/Y')}}</h2>
                    <p>Daily Pass</p>
                </div>

                <div class="row" data-aos="fade-up" data-aos-delay="200">

                @foreach($schedules as $s)

                   @for($i = 0; $i < count($s); $i++)
                        <?php 
                            $schedule = $s[$i];
                            $plan = $s[$i]['plan']; 
                        ?>
                        
                        <div class="col-lg-4 col-md-6 ">
                        <div class="card">
                            @if($plan->image)
                            <img src="{{asset('images/plan')}}/{{$plan->image}}" class="img-fluid card-img-top" alt="">
                            @else
                            <img src="{{asset('images/products/daily-pass-thumb.webp')}}" class="img-fluid card-img-top" alt="">
                            @endif
                            <div class="card-body">
                                <h4>{{ $plan->name }}</h4>
                                <p>{{ $plan->category->name}}</p>
                                <p>{{ $schedule['pax']}} left</p>
                                <div class="mb-3">
                                    <div class="" style="
                                            font-weight: bold;
                                            font-size: 1.3rem;
                                            float:right;
                                        ">IDR&nbsp;  {{$plan->price}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer contact d-flex">
                                <form action="{{url('basket')}}" method="POST" class="php-email-form">
                                    @csrf
                                    <input type="hidden" name="plan_id" value="{{$plan->id}}">
                                    <div class="align-items-lg-end d-flex justify-content-end">
                                        <div class="align-items-center d-flex form-group" style="margin-right: 1rem;padding: 0;">
                                            <label for="">Pax</label>
                                            <input type="number" name="qty" class="form-control px-4" id="pax" placeholder="Pax" style="height: 44px;margin-left: 10px;" min="1" required/>
                                              
                                        </div>
                                        @if(Carbon\Carbon::parse($date)->format('Y-m-d') <= Carbon\Carbon::now()->format('Y-m-d'))
                                            @if(Carbon\Carbon::now() < Carbon\Carbon::today()->addHour(19))
                                            <button type="submit" @if(!$schedule['available']) disabled @endif class="btn @if(!$schedule['available']) disabled @endif">@if(!$schedule['available']) Booked @else Book @endif</button>
                                            @else
                                            <button type="submit" class="btn disabled" disabled>Closed</button>
                                            @endif
                                        @else
                                            <button class="btn btn-primary @if(!$schedule['available']) disabled @endif"  @if(!$schedule['available']) disabled @endif type="submit" >@if(!$schedule['available']) Booked @else Book @endif</button>
                                        @endif

                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>

                    @endfor

                @endforeach

                </div>

            </div>
    </section>

</main><!-- End #main -->

@endsection


@push('script')
<script>
    $(document).ready(function() {
        $(function() {
            console.log($('#header'))
            $('#header').addClass('header-inner-pages')
        })
    });
</script>

@endpush