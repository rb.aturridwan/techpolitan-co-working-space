@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Category @if($isRecycleBin) <small>Recycle Bin</small> @endif
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Category</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Category</h3>

            <div style="float: right!important">
              @if(!$isRecycleBin)
              <a href="{{ route('admin.category.trashed') }}" class="btn btn-warning btn-sm">Recycle Bin</a>
              <a href="{{ route('admin.category.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Tambah Category Data</a>
              @endif

            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

@if(!$isRecycleBin)
<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@else
<form action="" method="get" id="restoreForm">
  @csrf
  @method("GET")
  <input type="submit" value="Restore" style="display: none">
</form>
@endif

@push('script')

    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '@if($isRecycleBin){{route('admin.category.dataTrashed')}}@else{{route('admin.category.data')}}@endif' ,
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'name' },
              { data: 'description' },
              { data: 'action' }
              
            ]
          })
        })
    </script>

@endpush
