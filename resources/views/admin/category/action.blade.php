
@if($model->deleted_at == null)
<a href="{{ route('admin.category.edit', $model) }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>

<button href="{{ route('admin.category.destroy', $model) }}" class="btn btn-danger" id="delete"><i class="fa fa-trash"></i></button>
@else
<button href="{{ route('admin.category.restore', $model) }}" class="btn btn-success" id="restore"><i class="fa fa-undo"></i></button>
@endif

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $('button#delete').on('click', function(e) {
        e.preventDefault();

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Apakah Yakin Hapus Data?',
            text: "Data yang sudah dihapus akan masuk ke tong sampah !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
        }).then((result) => {
            if (result.value) {

                document.getElementById('deleteForm').action = href;
                document.getElementById('deleteForm').submit();

                Swal.fire(
                    'Terhapus',
                    'Data kamu berhasil dihapus',
                    'success'
                )
            }
        })
    })

    $('button#restore').on('click', function(e) {

        e.preventDefault();

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Apakah Yakin Restore Data?',
            text: "Data akan dikembalikan dari tong sampah !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Restore'
        }).then((result) => {
            if (result.value) {

                document.getElementById('restoreForm').action = href;
                document.getElementById('restoreForm').submit();

                Swal.fire(
                    'Restored',
                    'Data kamu berhasil dikembalikan',
                    'success'
                )
            }
        })
          
    })
</script>