@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
      Category <small>Tambah Data</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('admin.category.index') }}"></i> Category</a></li>
      <li class="active">Tambah Data Category</li>
    </ol>
  </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- form multipart -->

            <form class="form-horizontal" action="{{ route('admin.category.store')}}" method="POST"  >
                @csrf
                <div class="box-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Category Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" placeholder="Masukan Nama User" value="{{ old('name') }}">
                            @error('name')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

               
                <!-- description field -->
                <div class="box-body">
                    <div class="form-group @error('description') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" placeholder="Masukan Description">{{ old('description') }}</textarea>
                            @error('description')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                <button type="submit" class="btn btn-info">Tambah Data</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('script')
<script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

@include('admin.template.partials.alert')

@endpush

