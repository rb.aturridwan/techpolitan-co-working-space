@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Private Office Request @if($isRecycleBin) <small>Recycle Bin</small> @endif
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Private Office Request</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Request Data</h3>

            <div style="float: right!important">

              <!-- <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-excel"></i>Download Excel</a> -->

            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Company</th>
                <th>Capacity</th>
                <th>Start Date</th>
                <th>Message</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>


@push('script')

    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('admin.private-office-request.data')}}' ,
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'name' },
              { data: 'phone' },
              { data: 'company_name' },
              { data: 'capacity' },
              { data: 'start_date' },
              { data: 'message' },
              { data: 'action' }
              
            ]
          })
        })
    </script>

@endpush
