<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('admin/dist/img/techpolitan-logo.jpeg') }}" class="img-circle" alt="User Image">
        </div>

        <div class="pull-left info">
          <p>{{ auth()->user()->name }}</p>
            <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!--<li class="header">MAIN NAVIGATION</li> -->


            <li class="{{ Route::currentRouteNamed('admin.dashboard')? 'active' : '' }}">
            <a href="{{ route('admin.dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
            </li>

            <li class="{{ Route::currentRouteNamed('admin.booking.index')? 'active' : '' }}">
            <a href="{{ route('admin.booking.index') }}">
                <i class="fa fa-suitcase"></i> <span>Booking</span>
            </a>
            </li>
            <li class="{{ Route::currentRouteNamed('admin.private-office-request.index')? 'active' : '' }}">
            <a href="{{ route('admin.private-office-request.index') }}">
                <i class="fa fa-comment"></i> <span>Private Office Request</span>
            </a>
            </li>

            <li class="{{ Route::currentRouteNamed('admin.plan.index')? 'active' : '' }}">
            <a href="{{ route('admin.plan.index') }}">
                <i class="fa fa-file"></i> <span>Plans</span>
            </a>
            </li>

            <li class="{{ Route::currentRouteNamed('admin.category.index')? 'active' : '' }}">
            <a href="{{ route('admin.category.index') }}">
                <i class="fa fa-database"></i> <span>Category</span>
            </a>
            </li>


            <li class="treeview {{ Route::currentRouteNamed('admin.user.index') || Route::currentRouteNamed('admin.user.employee')? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-user"></i>
                <span>Manage User</span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-list-alt"></i> Admin</a></li>
                <li><a href="{{ route('admin.user.employee') }}"><i class="fa fa-list-alt"></i> Customers</a></li>
            </ul>
            </li>


       <!-- <li class="header">MENU</li> -->
        <li>
          <a href="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="fa fa-sign-out"></i> Log Out</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
