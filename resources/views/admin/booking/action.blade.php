<a href="{{ route('admin.booking.invoice', $model) }}" target="_blank"  class="btn btn-primary"><i class="fa fa-eye"></i><span class="" style="margin-left:8px;font-size:10px;font-weight:bold">Invoice</span></a>


@if($model->status == 'Unpaid')
<button href="{{ route('admin.booking.confirm', $model) }}" style="font-size:10px;font-weight:bold" class="btn btn-success" id="confirm">Confirm Payment</i></button>
@endif

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $('button#confirm').on('click', function(e){
        e.preventDefault();

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Confirm Payment?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {

                document.getElementById('confirmForm').action = href;
                document.getElementById('booking_id').value = {{$model->id}};
                document.getElementById('confirmForm').submit();

                Swal.fire(
                'Confirmed',
                'Payment has confirmed',
                'success'
                )
            }
        })

    })
</script>
