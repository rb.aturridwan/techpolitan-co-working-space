@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Booking
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Booking</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Booking</h3>

            <div style="float: right!important">
              <a href="{{route('admin.booking.printall')}}" target="_blank" class="btn btn-primary btn-sm">Print Booking Data</a>
            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Order ID</th>
                <th>Plan</th>
                <th>Date</th>
                <th>Pax</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Payment Status</th>
                <th>Payment Proof</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

<form action="" method="post" id="confirmForm">
  @csrf
  @method("POST")
  <input type="hidden" name="booking_id" id="booking_id" value="" style="display: none">
  <input type="submit" value="Confirm" style="display: none">
</form>



@push('script')

    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.booking.data')}}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'order_id'},
              { data: 'plan' },
              { data: 'date' },
              { data: 'pax' },
              { data: 'price' },
              { data: 'status', render: function(data, type, row){
                console.log(data)
                  if(data.toLowerCase() == 'paid'){
                    return '<label class="badge bg-green d-block py-2">Paid</label>';
                  }else if(data.toLowerCase() == 'unpaid'){
                    return '<label class="badge bg-yellow d-block py-2">Unpaid</label>';
                  }else if(data.toLowerCase() == 'cancel'){
                    return '<label class="badge bg-reds d-block py-2">Cancel</label>';
                  }
                }
              },
              { data: 'payment_status', render: function(data, type, row){
                if(data == undefined || data == null){
                  return '<label class="badge bg-yellow d-block py-2">Waiting For Payments</label>';

                }
                  if(data.toLowerCase() == 'confirmed'){
                    return '<label class="badge bg-blue d-block py-2">Confirmed</label>';
                  }else if(data.toLowerCase() == 'waiting for confirmation'){
                    return '<label class="badge bg-green d-block py-2">Waiting For Confirmation</label>';
                  }else if(data.toLowerCase() == 'waiting for payment'){
                    return '<label class="badge bg-yellow d-block py-2">Waiting For Payments</label>';
                  }
                }
              },
              { data: 'payment_proof', render: function(data, type, row){
                  if(data == undefined || data == null){
                    return '-';
                  }else
                  {
                    return `<a href="{{url('/uploads/payment-proof')}}/${data}" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</a>`;
                  }
                }
              },
              { data: 'action', orderable: false, searchable: false },

            ]
          })
        })
    </script>

@endpush
