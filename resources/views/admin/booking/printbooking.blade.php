<html>

<head>
    <title>Techpolitan Space</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('backend/vendors/feather/feather.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('backend/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link href="{{asset('frontend/img/techpolitan.png')}}" rel="icon">


    <style>
        .invoice {
            min-height: 240px;
            font-family: 'Arial' !important;
            position: relative;
            width: 100%;
        }

        .table thead th,
        .table tbody td {
            font-size: 16px;
        }

        .rubber {
            box-shadow: 0 0 0 3px red, 0 0 0 2px #eaf5ec inset;
            border: 2px solid transparent;
            border-radius: 4px;
            display: inline-block;
            padding: 25px 5px;
            line-height: 22px;
            color: red;
            font-size: 4rem;
            text-transform: uppercase;
            text-align: center;
            opacity: 0.4;
            width: auto;
            transform: rotate(-5deg);
            position: absolute;
            top: 55px;
            right: 55px;
        }
    </style>




    <script src="{{asset('backend/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('backend/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/js/dataTables.select.min.js')}}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <!-- endinject -->
    <script src="{{asset('backend/js/off-canvas.js')}}"></script>
    <script src="{{asset('backend/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('backend/js/template.js')}}"></script>
    <script src="{{asset('backend/js/settings.js')}}"></script>
    <script src="{{asset('backend/js/todolist.js')}}"></script>

    <!-- Custom js for this page-->
    <script src="{{asset('backend/js/dashboard.js')}}"></script>
    <script src="{{asset('backend/js/Chart.roundedBarCharts.js')}}"></script>
</head>

<body>

    <div class="invoice d-flex mb-2">
        <!-- 90deg rotate badge -->
        <div class="container" >
            <div class="pb-2 row mb-5" style="border-bottom:2px solid #000">
                <div class="align-items-end col-9 d-flex px-0">
                    <h1 style="font-family:'Arial';font-weight:bolder;font-size: 60px;" class="mb-0">Bookings List</h1>
                </div>
                <div class="col-3 d-flex flex-column align-items-end">
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{asset('admin/dist/img/techpolitan-logo.jpeg')}}" height="50" class="card-img-left" alt="">
                        <span class="d-inline-block">Techpolitan</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0">
                    <table class="table ">
                        <thead style="background-color: #F5F7FF;">
                            <tr>
                                <th>
                                    <span>Order ID</span>
                                </th>
                                <th>
                                    <span>Plan</span>
                                </th>
                                <th>
                                    <span>Date</span>
                                </th>
                                <th>
                                    <span>Amount</span>
                                </th>
                                <th>
                                    <span>Qty</span>
                                </th>
                                <th>
                                    <span>Status</span>
                                </th>
                                <th>
                                    <span>Payment Status</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings as $booking)
                            <tr>
                                <td>
                                    <span>CW-{{$booking->user_id}}-{{$booking->id}}</span>
                                </td>
                                <td>
                                    <span>{{$booking->plan->name}}</span>
                                </td>
                                <td>
                                    <span>{{\Carbon\Carbon::parse($booking->date)->format('d M Y')}}</span>
                                </td>
                                <td>
                                    <span>IDR {{number_format($booking->plan->getRawOriginal('price') * $booking->pax, 2,",",".")}}</span>
                                </td>
                                <td>
                                    <span>{{$booking->pax}} pax</span>
                                </td>
                                <td>                                
                                    @if(strtolower($booking->status) == 'paid')
                                    <span >Paid</span>
                                    @elseif(strtolower($booking->status)== 'unpaid')
                                    <span>{{$booking->status}}</span>
                                    @elseif(strtolower($booking->status) == 'cancel')
                                    <span">{{$booking->status}}</span>

                                    @endif
                                </td>
                                <td>
                                    @if($booking->payment)                                
                                        @if(strtolower($booking->payment->status) == 'confirmed')
                                        <span >Confirmed</span>
                                        @elseif(strtolower($booking->payment->status)== 'waiting for confirmation')
                                        <span>Waiting for Confirmation</span>
                                        @elseif(strtolower($booking->payment->status) == 'waiting for payment')
                                        <span">Waiting For Payment</span>
                                        @endif
                                    @else
                                        <span">Waiting For Payment</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script>
        window.print();
    </script>
</body>


</html>