<!-- edit plan page -->

@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
        Plan <small>Edit Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.plan.index') }}"></i> Plan</a></li>
        <li class="active">Edit Data Plan</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Data Plan</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ route('admin.plan.update', $plan->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method("PUT")
                    <!-- input hidden id -->
                    <input type="hidden" name="id" value="{{ $plan->id }}">
                    <div class="box-body">
                        <div class="form-group @error('name') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Plan Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Masukan Nama User" value="{{ $plan->name }}">
                                @error('name')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group @error('price') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Price</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="price" placeholder="Masukan Price" value="{{ $plan->getRawOriginal('price') }}">
                                @error('price')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    

                    <!-- pax field -->
                    <div class="box-body">
                        <div class="form-group @error('pax') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Daily Max Pax</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="pax" placeholder="Masukan Pax" value="{{ $plan->pax }}">
                                @error('pax')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                    <!-- image field and show last image -->
                    <div class="box-body">
                        <div class="form-group @error('image') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Image</label>

                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="image" placeholder="Masukan Image" value="{{ $plan->image }}">
                                @error('image')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- show last image from db -->
                            <div class="col-sm-2">

                            </div>

                            <div class="col-sm-10">
                            @if($plan->image)
                            <img src="{{asset('images/plan')}}/{{$plan->image}}" width="200px" alt="">
                            @endif
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- category id field  -->
                    <div class="box-body">
                        <div class="form-group @error('category_id') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10">
                                <select name="category_id" class="form-control">
                                    <option value="">Pilih Category</option>
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $plan->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                     <!-- description field -->
                     <div class="box-body">
                        <div class="form-group @error('description') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" placeholder="Masukan Description">{{ $plan->description }}</textarea>
                                @error('description')
                                <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <a href="{{ route('admin.plan.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


@endsection