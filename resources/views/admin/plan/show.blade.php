<!-- edit plan page -->

@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
        Plan <small>Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.plan.index') }}"></i> Plan</a></li>
        <li class="active">Data Plan - # {{$plan->id}}</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Plan</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                    <div class="box-body">
                        <div class="form-group @error('name') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Plan Name</label>

                            <div class="col-sm-10">
                                {{ $plan->name }}
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group @error('price') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Price</label>

                            <div class="col-sm-10">
                                IDR {{ $plan->price }}
                            </div>
                        </div>
                    </div>
                    

                    <!-- pax field -->
                    <div class="box-body">
                        <div class="form-group @error('pax') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Daily Max Pax</label>

                            <div class="col-sm-10">
                                {{ $plan->pax }} pax
                            </div>
                        </div>
                    </div>

                    <!-- image field and show last image -->
                    <div class="box-body">
                        <div class="form-group @error('image') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Image</label>

                            <div class="col-sm-10">
                            @if($plan->image)
                            <img src="{{asset('images/plan')}}/{{$plan->image}}" width="200px" alt="">
                            @else
                            -
                            @endif
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- category id field  -->
                    <div class="box-body">
                        <div class="form-group @error('category_id') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Category</label>

                            <div class="col-sm-10">
                                {{ $plan->category->name }}
                            </div>
                        </div>
                    </div>

                     <!-- description field -->
                     <div class="box-body">
                        <div class="form-group @error('description') has-error @enderror">
                            <label for="" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                {{ $plan->description }}
                            </div>
                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <a href="{{ route('admin.plan.index') }}" class="btn btn-default">Cancel</a>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


@endsection