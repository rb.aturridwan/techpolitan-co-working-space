@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Plan @if($isRecycleBin) <small>Recycle Bin</small> @endif
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Plan</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Plan</h3>

            <div style="float: right!important">
              @if(!$isRecycleBin)
              <a href="{{ route('admin.plan.trashed') }}" class="btn btn-warning btn-sm">Recycle Bin</a>
              <a href="{{ route('admin.plan.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Tambah Plan Data</a>
              @endif

            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>Image</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

@if(!$isRecycleBin)
<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@else
<form action="" method="get" id="restoreForm">
  @csrf
  @method("GET")
  <input type="submit" value="Restore" style="display: none">
</form>
@endif

@push('script')

    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '@if($isRecycleBin){{route('admin.plan.dataTrashed')}}@else{{route('admin.plan.data')}}@endif' ,
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'name' },
              { data: 'category' },
              { data: 'price' },
              { data: 'image', render:function(data, type, row){
                if(data != null && data != ''){
                  return `<img src="{{url('/')}}/images/plan/${data}" width="100px">`;
                }
                else{
                  return `<img src="{{asset('images/products/daily-pass-thumb.webp')}}" width="100px">`;
                }
              } },
              { data: 'description' },
              { data: 'action' }
              
            ]
          })
        })
    </script>

@endpush
