@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
      Plan <small>Tambah Data</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('admin.plan.index') }}"></i> Plan</a></li>
      <li class="active">Tambah Data Plan</li>
    </ol>
  </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Plan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- form multipart -->

            <form class="form-horizontal" action="{{ route('admin.plan.store')}}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Plan Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" placeholder="Masukan Nama User" value="{{ old('name') }}">
                            @error('name')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('price') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Price</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="price" placeholder="Masukan Price" value="{{ old('price') }}">
                            @error('price')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('pax') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Daily Max Pax</label>

                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="pax" placeholder="Masukan Max Pax" value="{{ old('pax') }}" />
                            @error('pax')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!-- image field -->

                <div class="box-body">
                    <div class="form-group @error('image') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Image</label>

                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="image" placeholder="Masukan Image" value="{{ old('image') }}" />
                            @error('image')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!-- category field -->
                <div class="box-body">
                    <div class="form-group @error('category_id') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Category</label>

                        <div class="col-sm-10">
                            <select name="category_id" class="form-control">
                                <option value="">Pilih Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!-- description field -->
                <div class="box-body">
                    <div class="form-group @error('description') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" placeholder="Masukan Description">{{ old('description') }}</textarea>
                            @error('description')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                <button type="submit" class="btn btn-info">Tambah Data</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection

@push('script')
<script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>

@include('admin.template.partials.alert')

@endpush

