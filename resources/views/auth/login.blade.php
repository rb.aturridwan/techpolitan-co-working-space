@extends('auth.layouts.auth')

@section('content')      
		<h4>Hello! let's get started</h4>
		<h6 class="font-weight-light">Sign in to continue.</h6>
		<form class="pt-3" method="POST" action="{{ route('login') }}">
		@csrf
		<div class="form-group">
			<input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" value="{{ old('email') }}" required>
				@error('email')
					<span class="invalid-feedback d-block" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror   
		</div>
		<div class="form-group">
			<input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" required>
				@error('password')
					<span class="invalid-feedback d-block" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
		</div>
		<div class="mt-3">
			<button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="">SIGN IN</button>
		</div>
		<div class="my-2 d-flex justify-content-between align-items-center">
			<div class="form-check">
			<label class="form-check-label text-muted">
				<input type="checkbox" name="remember" id="remember" class="form-check-input" {{ old('remember') ? 'checked' : '' }}>
				{{ __('Remember Me') }}
			</label>
			</div>
			<a href="#" class="auth-link text-black">Forgot password?</a>
		</div>
		<div class="text-center mt-4 font-weight-light">
			Don't have an account? <a href="{{url('/register')}}" class="text-primary">{{__('Register')}}</a>
		</div>
		</form>
@endsection
