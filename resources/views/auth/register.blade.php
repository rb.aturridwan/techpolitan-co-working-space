@extends('auth.layouts.auth')

@section('content')
		<h4>New here?</h4>
		<h6 class="font-weight-light">Signing up is easy. It only takes a few steps</h6>
		<form class="pt-3" method="POST" action="{{ route('register') }}">
			@csrf
			<div class="form-group">
			<input type="text" name="name" class="form-control form-control-lg" id="exampleInputUsername1" placeholder="Name" value="{{ old('name') }}" required>
				@error('name')
					<span class="d-block invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror   
			</div>
			<div class="form-group">
			<input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" value="{{ old('email') }}" required>
				@error('email')
					<span class="d-block invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror   
			</div>
			<div class="form-group">
			<input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password"  required>
				@error('password')
					<span class="d-block invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
			</div>
			<div class="form-group">
			<input type="password" name="password_confirmation" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Confirm Password"  autocomplete="new-password" required>
				@error('password_confirmation')
					<span class="d-block invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
				@enderror
			</div>
			<div class="mb-4">
			<div class="form-check">
				<label class="form-check-label text-muted">
				<input type="checkbox" class="form-check-input">
				I agree to all Terms & Conditions
				</label>
			</div>
			</div>
			<div class="mt-3">
			<button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" >
				{{ __('Register') }}
			</button>
			</div>
			<div class="text-center mt-4 font-weight-light">
			Already have an account? <a href="{{url('login')}}" class="text-primary">Login</a>
			</div>
		</form>
@endsection

