@extends('layouts.app')

@section('content')


<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

        <div class="row " data-aos="fade-up" data-aos-delay="150">
            <div class="col-xl-6 col-lg-8">
                <h1>Temukan ruang yang paling cocok</h1>
                <h2>Mencari ruang yang nyaman untuk bekerja sendiri atau dengan tim? Kami punya pilihan space untukmu.</h2>
            </div>
        </div>

        <div class="row gy-4 mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
        </div>

    </div>
</section><!-- End Hero -->

<div class="card landing-card-form landing-card-form-desktop floating ml-auto d-none d-lg-inline-block">
    @if (session()->has('message'))

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Success</strong> Your message submitted.
    </div>
    @endif
    <ul class="nav nav-tabs d-flex" id="bookingMenu" role="tablist">
        <li class="nav-item w-50" role="presentation">
            <a class="nav-link active text-center" id="coworking-tab" data-toggle="tab" href="#coworking" role="tab" aria-controls="coworking" aria-selected="true">Daily Pass</a>
        </li>
        <li class="nav-item w-50" role="presentation">
            <a class="nav-link text-center" id="privateOffice-tab" data-toggle="tab" href="#privateOffice" role="tab" aria-controls="private-office" aria-selected="false">Private Office</a>
        </li>

    </ul>
    <div class="tab-content contact" id="bookingMenuContent">
        <div class="tab-pane fade p-4 show active" id="coworking" role="tabpanel" aria-labelledby="coworking">
            <form action="{{url('search-coworking')}}" method="GET" role="form" class="php-email-form">

                <div class="form-group mt-3">
                    <input type="date" class="form-control" name="date" id="date" value="{{now()->format('Y-m-d')}}" placeholder="Search Schedule">

                </div>
                <div class="form-group mt-3 mb-3">
                    <input type="number" class="form-control" name="pax" id="pax" placeholder="Pax">

                </div>
                <div class=""><button type="submit">Search</button></div>
            </form>
        </div>
        <div class="tab-pane fade p-4" id="privateOffice" role="tabpanel" aria-labelledby="private-office">
            <form action="{{url('private-office-message')}}" method="post" role="form" class="php-email-form">
                @csrf
                <div class="form-group mt-3">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="form-group mt-3">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
                <div class="form-group mt-3">
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Your Phone" required>
                </div>
                <div class="form-group mt-3">
                    <input type="phone" class="form-control" name="company_name" id="company" placeholder="Your company" required>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6 form-group">
                        <input type="number" class="form-control" name="capacity" id="capacity" placeholder="Capacity" required>
                    </div>
                    <div class="col-md-6 form-group mt-3 mt-md-0">
                        <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Date" value="{{now()->format('Y-m-d')}}" required>
                    </div>
                </div>
                <div class="form-group mt-3 mb-3">
                    <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                </div>

                <div class=""><button type="submit">Send Message</button></div>
            </form>
        </div>
    </div>
    <style>
        .landing-card-form-desktop.floating {
            display: inline-block;
            position: absolute;
            top: 20vh;
            right: 8%;
            background: #fff;
            z-index: 2;
        }

        .landing-card-form-desktop {
            width: 400px;
            height: auto;
            margin-top: 60px;
        }

        .landing-card-form input+label {
            font-size: 14px !important;
            white-space: nowrap;
        }

        .react-datepicker-popper {
            z-index: 3;
        }

        #bookingMenu a.active {
            font-weight: bold;
            color: black;
            background-color: #fff;
            border-color: #fff;

        }

        #bookingMenu a {
            color: #6c757d;
            background-color: #cecaca45;
            border-color: #cecaca45;
        }
    </style>
</div>

<main id="main">


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Contact</h2>
                <p>Contact Us</p>
            </div>

            <div>
                <div class="maprouter">
                    <div class="gmap_canvas"><iframe style="width: 100%;height:270px;" id="gmap_canvas" src="https://maps.google.com/maps?q=techpolitan%20bsd&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.whatismyip-address.com"></a><br>
                        <style>
                            .mapouter {
                                position: relative;
                                text-align: right;
                                height: 270px;
                                width: 100%;
                            }
                        </style>
                        <style>
                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 270px;
                                width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>

            <div class="row mt-5">

                <div class="col-lg-12">
                    <div class="info d-flex justify-content-around">
                        <div class="address">
                            <i class="bi bi-geo-alt"></i>
                            <h4>Location:</h4>
                            <p>Gd. Cinema Unit L81-82 <br>
                                The Breeze BSD City, Sampora<br>
                                Tangerang, Banten</p>
                        </div>

                        <div class="email mt-0">
                            <i class="bi bi-envelope"></i>
                            <h4>Email:</h4>
                            <p>info@techpolitan.co</p>
                        </div>

                        <div class="phone mt-0">
                            <i class="bi bi-phone"></i>
                            <h4>Call:</h4>
                            <p> (021) 29580058</p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

@endsection

@push('script')
<script>
    $(function() {
        $('#bookingMenu a').on('click', function(event) {
            event.preventDefault()
            $(this).tab('show')
        })

        console.log($(document).find('#bookingMenu a'))
    })

    // dismiss alert
    $('.alert').on('click', function() {
        $(this).fadeOut();
    });

    // dissmiss alert after 5 seconds
    setTimeout(function() {
        $('.alert').fadeOut();
    }, 5000);
</script>

@endpush