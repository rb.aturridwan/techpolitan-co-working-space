@extends('user/layouts/app')

@push('css')

<style>
    .e-voucher {
        min-height: 240px;
        border: 1px solid #000;
        border-left: 40px solid #000;
        position: relative;
        width: 100%;
    }

    .e-voucher .e-voucher-badge {

        font-weight: bold;
        color: white;
        text-transform: uppercase;
        letter-spacing: 3px;
        position: absolute;
        bottom: 25px;
        left: 0;
        margin-left: -30px;
        -webkit-transform: rotate(270deg);
        -moz-transform: rotate(270deg);
        -ms-transform: rotate(270deg);
        -o-transform: rotate(270deg);
        transform: rotate(270deg);
        -webkit-transform-origin: 0 0;
        -moz-transform-origin: 0 0;
        -ms-transform-origin: 0 0;
        -o-transform-origin: 0 0;
        transform-origin: 0 0;
    }
</style>
@endpush

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">My Plan</a></li>
        <li class="breadcrumb-item"><a href="#">E-Voucher</a></li>
        <li class="breadcrumb-item active" aria-current="page"># {{$booking->id}}</li>
    </ol>
</nav>



<div class="row my-5">
    <div class="col-12">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-2 d-flex">
                    <div class="d-flex align-items-center">
                        <strong>{{$booking->plan->name}} - #{{$booking->id}} - {{$booking->pax}} pax</strong>
                    </div>
                    <button type="button" id="btnEvoucher" href="{{route('user-print-evoucher', $booking)}}" class="ml-auto btn btn-outline-info btn-icon-text">
                        Print
                        <i class="ti-printer btn-icon-append"></i>
                    </button>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="d-flex align-items-center card-body p-3 px-3 flex-column">
                            @for($i=0;$i<$booking->pax;$i++)
                                <div class="e-voucher d-flex mb-2">
                                    <!-- 90deg rotate badge -->
                                    <div class="d-flex e-voucher-badge">
                                        <span>TECHPOLITAN</span>
                                    </div>
                                    <div class="w-75 p-4">
                                        <h4>E-VOUCHER CO-WORKING SPACE</h4>
                                        <h3>{{$booking->plan->name}}</h3>
                                        <p>#CW-{{$booking->user_id}}-{{$booking->id}}-{{$i+1}} - {{Carbon\Carbon::parse($booking->date)->format('d/m/Y')}}</p>
                                        <ul class="mb-0">
                                            <li>Open start 9am - 7pm</li>
                                            <li>Please show this e-voucher to our staff</li>
                                            <li>E-Voucher can be used to redeem a mineal water</li>
                                            <li>E-Voucher are nob refundable </li>
                                        </ul>
                                    </div>
                                    <div class="w-25 p-4 d-flex justify-content-center align-items-center" style="border-left: 1px solid;">
                                        <img src="{{asset('admin/dist/img/techpolitan-logo.jpeg')}}" height="130" class="card-img-left" alt="">
                                    </div>
                                </div>
                                @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- content -->

@endsection

@push('js')

<script>
    $(document).ready(function() {
        $(document).find('#myPlanMenu').addClass('active');

        $('#btnEvoucher').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            
            window.open(href, '_blank').focus();
        });
    });
</script>

@endpush