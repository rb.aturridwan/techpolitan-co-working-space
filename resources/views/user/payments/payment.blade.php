@extends('user/layouts/app')

@push('css')

<link rel="stylesheet" href="{{asset('backend/vendors/jquery-file-upload/uploadfile.css')}}">

<style>
    .ajax-file-upload{
        background-color: #4B49AC !important;
        color:#fff !important;
    }
</style>
@endpush

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Payment</li>
    </ol>
</nav>



<div class="row mt-5">
    <div class="col-12">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="d-flex align-items-center card-body p-3 px-3">
                            <div class="col-9 border-right pr-0">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6 mb-3">
                                            <strong>Date</strong>
                                        </div>
                                        <div class="col-3">
                                            <strong>Amount</strong>
                                        </div>
                                        <div class="col-3">
                                            <strong>Due</strong>
                                        </div>

                                    </div>
                                    <div class="row mb-3 border-bottom">
                                        <div class="col-6">
                                            <strong>#{{$booking->invoice_no}} - {{Carbon\Carbon::parse($booking->date)->format('d/m/Y')}}</strong><br>
                                            <p>To be paid by {{Carbon\Carbon::parse($booking->date)->format('d/m/Y')}}</p>
                                        </div>
                                        <div class="col-3">
                                            <span>IDR {{$booking->price}}</span>
                                        </div>
                                        <div class="col-3">
                                            <span>{{Carbon\Carbon::parse($booking->date)->format('d M Y')}}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <img src="{{asset('images/products/daily-pass-thumb.webp')}}" height="100" class="card-img-left" alt="">

                                        </div>
                                        <div class="col-3">
                                            <h6 class="card-title my-2">{{$plan->name}}</h6>
                                            <p>{{$plan->category->name}}</p>
                                            <strong>{{$booking->pax}} pax</strong>
                                        </div>
                                        <div class="col-6">
                                            <input type="hidden" name="booking_id" id="booking_id" value="{{$booking->id}}">
                                            <div class="file-upload-wrapper">
                                                <div id="fileuploader">Upload Payment Proof</div>
                                            </div>
                                            <div id="payment-proof-uploaded"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-3 card h-100">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="d-flex card-body p-3 px-3">
                                                    <div class="ml-3">
                                                        <h5 class="card-title mb-3">Transfer to</h5>
                                                        <p class="card-text">
                                                            <strong>Bank Account Name</strong>
                                                            <br>

                                                            <span>PT. Techpolitan</span>
                                                            <br>
                                                            <br>
                                                            <strong>Bank Account Number</strong>
                                                            <br>
                                                            <span>123456789</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<!-- link back -->
<div class="row mt-3">
    <div class="col-12">
        <div class="container">
        <a href="{{route('user-myplan')}}" class="btn btn-outline-primary py-2"> <i style="font-size: 10px;" class="icon-arrow-left"></i> Back</a>
            
        </div>
    </div>
</div>


<!-- content -->

@endsection

@push('js')

<script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('backend/vendors/jquery-file-upload/jquery.uploadfile.min.js')}}"></script>
<script src="{{asset('backend/js/jquery-file-upload.js')}}"></script>


<script>
    $(document).ready(function() {
        $(document).find('#paymentMenu').addClass('active');
    });
</script>

@endpush