<html>

<head>
    <title>Techpolitan Space</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('backend/vendors/feather/feather.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('backend/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link href="{{asset('frontend/img/techpolitan.png')}}" rel="icon">


    <style>
        .invoice {
            min-height: 240px;
            font-family: 'Arial' !important;
            position: relative;
            width: 100%;
        }

        .table thead th,
        .table tbody td {
            font-size: 16px;
        }

        .rubber {
            box-shadow: 0 0 0 3px red, 0 0 0 2px #eaf5ec inset;
            border: 2px solid transparent;
            border-radius: 4px;
            display: inline-block;
            padding: 25px 5px;
            line-height: 22px;
            color: red;
            font-size: 4rem;
            text-transform: uppercase;
            text-align: center;
            opacity: 0.4;
            width: auto;
            transform: rotate(-5deg);
            position: absolute;
            top: 55px;
            right: 55px;
        }
    </style>




    <script src="{{asset('backend/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('backend/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/js/dataTables.select.min.js')}}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <!-- endinject -->
    <script src="{{asset('backend/js/off-canvas.js')}}"></script>
    <script src="{{asset('backend/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('backend/js/template.js')}}"></script>
    <script src="{{asset('backend/js/settings.js')}}"></script>
    <script src="{{asset('backend/js/todolist.js')}}"></script>

    <!-- Custom js for this page-->
    <script src="{{asset('backend/js/dashboard.js')}}"></script>
    <script src="{{asset('backend/js/Chart.roundedBarCharts.js')}}"></script>
</head>

<body>

    <div class="invoice d-flex mb-2">
        <!-- 90deg rotate badge -->
        <div class="container" >
            <div class="pb-2 row mb-5" style="border-bottom:2px solid #000">
                <div class="align-items-end col-9 d-flex px-0">
                    <h1 style="font-family:'Arial';font-weight:bolder;font-size: 60px;" class="mb-0">INVOICE</h1>
                </div>
                <div class="col-3 d-flex flex-column align-items-end">
                    <div class="d-flex flex-column align-items-center">
                        <img src="{{asset('admin/dist/img/techpolitan-logo.jpeg')}}" height="50" class="card-img-left" alt="">
                        <span class="d-inline-block">Techpolitan</span>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-6 pl-0">
                    <div class="d-flex flex-column">
                        <span>Invoice to:</span>
                        <h3 class="mt-2">{{$booking->user->name}}</h3>
                    </div>
                </div>
                <div class="col-6 pl-0">
                    <div class="d-flex flex-column">
                        <div class="container">
                            <div class="row">
                                <div class="col-3">
                                    <span>Invoice No</span>
                                </div>
                                <div class="col-8">
                                    <span>: #{{$booking->invoice_no}}</span>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-3">
                                    <span>Invoice Date</span>
                                </div>
                                <div class="col-8">
                                    <span>: {{Carbon\Carbon::parse($booking->date)->format('d M Y')}}</span>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 px-0">
                    <table class="table ">
                        <thead style="background-color: #F5F7FF;">
                            <tr>
                                <th>
                                    <span>Description</span>
                                </th>
                                <th>
                                    <span>Price</span>
                                </th>
                                <th>
                                    <span>Qty</span>
                                </th>
                                <th>
                                    <span>Subtotal</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span>{{$booking->plan->name}}</span>
                                </td>
                                <td>
                                    <span>IDR {{$booking->plan->price}}</span>
                                </td>
                                <td>
                                    <span>{{$booking->pax}}</span>
                                </td>
                                <td>
                                    <span>IDR {{number_format($booking->plan->getRawOriginal('price') * $booking->pax, 2,",",".")}}</span>
                                </td>
                            </tr>
                            <tr style="background-color: #F5F7FF;">
                                <td>
                                    <strong style="font-size:1rem">Total</strong>
                                </td>
                                <td>
                                    <span>IDR {{$booking->plan->price}}</span>
                                </td>
                                <td>
                                    <span>{{$booking->pax}}</span>
                                </td>
                                <td>
                                    <span>IDR {{number_format($booking->plan->getRawOriginal('price') * $booking->pax, 2,",",".")}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>
                                    <span>Service Fee :</span>
                                </td>
                                <td>
                                    <span>IDR {{number_format(($booking->plan->getRawOriginal('price') * $booking->pax) * 0.1, 2, ",",".")}}</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-0" style="border-top:0px solid #fff !important">

                                </td>
                                <td class="py-0" style="border-top:0px solid #fff !important">

                                </td>
                                <td class="py-0" style="border-top:0px solid #fff !important">
                                    <span>Total :</span>
                                </td>
                                <td class="py-0" style="border-top:0px !important">
                                    <span>IDR {{$booking->price}}</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if($booking->status == 'Paid')
            <div class="rubber">
                PAID
            </div>
            @endif
            <div class="row justify-content-end mt-4">
                <div class="col-3 d-flex px-0">
                    <div class="d-flex flex-column w-100 " style="min-height: 150px;">
                        <div style="border-top: 1px solid #000;" class="d-flex justify-content-center mt-auto p-1 w-50">
                            <strong>Finance</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        window.print();
    </script>
</body>


</html>