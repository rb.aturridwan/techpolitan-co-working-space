@extends('user/layouts/app')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Basket</li>
    </ol>
</nav>



<div class="row my-5">
    <div class="col-8">
        <div class="container">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="card">
                        <div class="d-flex align-items-center card-body p-3 px-3">
                            <img src="{{asset('images/products/daily-pass-thumb.webp')}}" height="100" class="card-img-left" alt="">
                            <div class="ml-3">
                                <h5 class="card-title mb-1">{{$schedule['plan']->name}}</h5>
                                <p>{{$schedule['plan']->category->name}}</p>

                                <p class="card-text">
                                    <span class="text-muted">Date:</span>
                                    <span class="text-primary">{{$schedule['date']}}</span>
                                    <br>
                                    <span class="text-muted">Pax:</span>
                                    <span class="text-primary">{{$schedule['pax']}}</span>
                                    <br>
                                    <span class="text-muted">Price:</span>
                                    <span class="text-primary">IDR {{$schedule['plan']->price}}</span>
                                </p>
                            </div>
                            <!-- remove button -->
                            <div class="d-flex justify-content-center align-items-center ml-auto">
                                <button class="btn btn-outline-danger">
                                    <i class="icon-trash"></i>
                                    Remove
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-4">
        <!-- display total -->
        <div class="container">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="card">
                        <div class="d-flex flex-column card-body p-3 px-3">
                            <div class="ml-3">
                                <h5 class="card-title text-muted">Sub Total</h5>
                                <h5 class="card-title">IDR {{$schedule['subtotal']}}</h5>

                                <h5 class="card-title text-muted">Tax</h5>
                                <h5 class="card-title">IDR {{$schedule['tax']}}</h5>

                                <h5 class="card-title text-muted">Total To Pay</h5>
                                <h5 class="card-title">IDR {{$schedule['total']}}</h5>

                                <form action="{{url('proceedpayment')}}" method="POST">
                                    @csrf

                                    <input type="hidden" name="date" value="{{$schedule['date']}}">
                                    <input type="hidden" name="qty" value="{{$schedule['pax']}}">
                                    <input type="hidden" name="plan_id" value="{{$schedule['plan']->id}}">
                                    <input type="hidden" name="total" value="{{$schedule['total']}}">
                                    <button class="btn btn-primary">
                                        Proceed to Payment
                                    </button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- content -->

@endsection

@push('js')

<script>
    $(document).ready(function() {
        $(document).find('#serviceMenu').addClass('active');
    });
</script>

@endpush