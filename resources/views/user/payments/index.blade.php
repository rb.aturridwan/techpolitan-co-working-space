@extends('user/layouts/app')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Payments</li>
    </ol>
</nav>

<div class="card">
            <div class="card-body">
              <h4 class="card-title">Invoice And Payments</h4>
              <div class="row">
                <div class="col-12">
                <div class="table">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Order #</th>
                            <th>Invoice</th>
                            <th>Due Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!-- <tr>
                            <td>1</td>
                            <td>Daily Pass Reguler</td>
                            <td>9 Jul 2022</td>
                            <td>2</td>
                            <td>IDR 75.000</td>
                            <td>
                              <label class="badge badge-warning d-block py-2">Unpaid</label>
                            </td>
                            <td>
                              <button class="btn btn-outline-primary py-2">Invoice</button>
                              <button class="btn btn-outline-primary py-2 disabled" disabled>E-Voucher</button>
                            </td>
                        </tr> -->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>


<!-- content -->

@endsection

@push('js')

    <script>
        $(document).ready(function() {
            $(document).find('#paymentMenu').addClass('active');
        });

        (function($) {
  'use strict';
  $(function() {
    $('#order-listing').DataTable({
      processing:true,
      serverSide:true,
            ajax: '{{ route('admin.payment.data')}}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'invoice_no' },
              { data: 'date' },
              { data: 'price', render: function(data, type, row, meta) {
                return 'IDR ' + data;
              } },
              { data: 'status', render: function(data, type, row){
                console.log(data)
                  if(data.toLowerCase() == 'paid'){
                    return '<label class="badge badge-success d-block py-2">Paid</label>';
                  }else if(data.toLowerCase() == 'unpaid'){
                    return '<label class="badge badge-warning d-block py-2">Unpaid</label>';
                  }else if(data.toLowerCase() == 'cancel'){
                    return '<label class="badge badge-danger d-block py-2">Cancel</label>';
                  }
                }
              },
              { data: 'action' }
              
            ],
      "aLengthMenu": [
        [5, 10, 15, -1],
        [5, 10, 15, "All"]
      ],
      "iDisplayLength": 5,
      "language": {
        search: ""
      }
    });
    $('#order-listing').each(function() {
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.removeClass('form-control-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.removeClass('form-control-sm');
    });
  });
})(jQuery);
    </script>

@endpush