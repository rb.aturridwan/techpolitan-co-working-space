<a href="{{route('user-payment-invoice',$model)}}" class="btn btn-outline-primary py-2">View</a>
@if($model->status == 'Unpaid')
<form action="{{route('user-pay')}}" method="POST">
    @csrf
    <input type="hidden" name="id" value="{{ $model->id }}">
    <button class="btn btn-primary py-2">Pay With Bank Transfer</button>
</form>
@endif