@extends('user/layouts/app')

@push('css')

<style>
    .invoice {
        min-height: 240px;
        font-family: 'Arial' !important;
        position: relative;
        width: 100%;
    }

    .table thead th,
    .table tbody td {
        font-size: 16px;
    }

    .rubber {
        box-shadow: 0 0 0 3px red, 0 0 0 2px #eaf5ec inset;
        border: 2px solid transparent;
        border-radius: 4px;
        display: inline-block;
        padding: 25px 5px;
        line-height: 22px;
        color: red;
        font-size: 4rem;
        text-transform: uppercase;
        text-align: center;
        opacity: 0.4;
        width: auto;
        transform: rotate(-5deg);
        position: absolute;
        top: 55px;
        right: 55px;
    }
</style>
@endpush

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Payments</a></li>
        <li class="breadcrumb-item"><a href="#">Invoice</a></li>
        <li class="breadcrumb-item active" aria-current="page"># {{$booking->invoice_no}}</li>
    </ol>
</nav>



<div class="row my-5">
    <div class="col-12">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-2 d-flex">
                    <div class="d-flex align-items-center">
                        <strong>{{$booking->plan->name}} - #{{$booking->invoice_no}} </strong>
                    </div>
                    <button type="button" id="btnEvoucher" href="{{route('user-print-invoice', $booking)}}" class="ml-auto btn btn-outline-info btn-icon-text">
                        Print
                        <i class="ti-printer btn-icon-append"></i>
                    </button>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="d-flex align-items-center card-body p-5 px-3 flex-column">

                            <div class="invoice d-flex mb-2">
                                <!-- 90deg rotate badge -->
                                <div class="container">
                                    <div class="pb-2 row mb-5" style="border-bottom:2px solid #000">
                                        <div class="align-items-end col-9 d-flex px-0">
                                            <h1 style="font-family:'Arial';font-weight:bolder;font-size: 60px;" class="mb-0">INVOICE</h1>
                                        </div>
                                        <div class="col-3 d-flex flex-column align-items-end">
                                            <div class="d-flex flex-column align-items-center">
                                                <img src="http://127.0.0.1:8000/admin/dist/img/techpolitan-logo.jpeg" height="50" class="card-img-left" alt="">
                                                <span class="d-inline-block">Techpolitan</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-5">
                                        <div class="col-7 pl-0">
                                            <div class="d-flex flex-column">
                                                <span>Invoice to:</span>
                                                <h3 class="mt-2">{{$booking->user->name}}</h3>
                                            </div>
                                        </div>
                                        <div class="col-5 pl-0">
                                            <div class="d-flex flex-column">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <span>Invoice No</span>
                                                        </div>
                                                        <div class="col-8">
                                                            <span>: #{{$booking->invoice_no}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2">
                                                        <div class="col-4">
                                                            <span>Invoice Date</span>
                                                        </div>
                                                        <div class="col-8">
                                                            <span>: {{Carbon\Carbon::parse($booking->date)->format('d M Y')}}</span>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 px-0">
                                            <table class="table ">
                                                <thead style="background-color: #F5F7FF;">
                                                    <tr>
                                                        <th>
                                                            <span>Description</span>
                                                        </th>
                                                        <th>
                                                            <span>Price</span>
                                                        </th>
                                                        <th>
                                                            <span>Qty</span>
                                                        </th>
                                                        <th>
                                                            <span>Subtotal</span>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <span>{{$booking->plan->name}}</span>
                                                        </td>
                                                        <td>
                                                            <span>IDR {{$booking->plan->price}}</span>
                                                        </td>
                                                        <td>
                                                            <span>{{$booking->pax}}</span>
                                                        </td>
                                                        <td>
                                                            <span>IDR {{number_format($booking->plan->getRawOriginal('price') * $booking->pax, 2,",",".")}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr style="background-color: #F5F7FF;">
                                                        <td>
                                                            <strong style="font-size:1rem">Total</strong>
                                                        </td>
                                                        <td>
                                                            <span>IDR {{$booking->plan->price}}</span>
                                                        </td>
                                                        <td>
                                                            <span>{{$booking->pax}}</span>
                                                        </td>
                                                        <td>
                                                            <span>IDR {{number_format($booking->plan->getRawOriginal('price') * $booking->pax, 2,",",".")}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>

                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            <span>Service Fee :</span>
                                                        </td>
                                                        <td>
                                                            <span>IDR {{number_format(($booking->plan->getRawOriginal('price') * $booking->pax) * 0.1, 2, ",",".")}}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="py-0" style="border-top:0px solid #fff !important">

                                                        </td>
                                                        <td class="py-0" style="border-top:0px solid #fff !important">

                                                        </td>
                                                        <td class="py-0" style="border-top:0px solid #fff !important">
                                                            <span>Total :</span>
                                                        </td>
                                                        <td class="py-0" style="border-top:0px !important">
                                                            <span>IDR {{$booking->price}}</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @if($booking->status == 'Paid')
                                    <div class="rubber">
                                        PAID
                                    </div>
                                    @endif
                                    <div class="row justify-content-end mt-4">
                                        <div class="col-3 d-flex px-0">
                                            <div class="d-flex flex-column w-100 " style="min-height: 150px;">
                                                <div style="border-top: 1px solid #000;" class="d-flex justify-content-center mt-auto p-1 w-50">
                                                    <strong>Finance</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- content -->

@endsection

@push('js')

<script>
    $(document).ready(function() {
        $(document).find('#myPlanMenu').addClass('active');

        $('#btnEvoucher').click(function(e) {
            e.preventDefault();

            let href = $(this).attr('href');
            
            window.open(href, '_blank').focus();

        });
    });
</script>

@endpush