<html>

<head>
    <title>Techpolitan Space</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('backend/vendors/feather/feather.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('backend/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('backend/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link href="{{asset('frontend/img/techpolitan.png')}}" rel="icon">


    <style>
        .e-voucher {
            min-height: 240px;
            border: 1px solid #000;
            border-left: 40px solid #000;
            position: relative;
            width: 100%;
        }

        .e-voucher .e-voucher-badge {

            font-weight: bold;
            color: white;
            text-transform: uppercase;
            letter-spacing: 3px;
            position: absolute;
            bottom: 25px;
            left: 0;
            margin-left: -30px;
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            transform: rotate(270deg);
            -webkit-transform-origin: 0 0;
            -moz-transform-origin: 0 0;
            -ms-transform-origin: 0 0;
            -o-transform-origin: 0 0;
            transform-origin: 0 0;
        }
    </style>




    <script src="{{asset('backend/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('backend/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('backend/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('backend/js/dataTables.select.min.js')}}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <!-- endinject -->
    <script src="{{asset('backend/js/off-canvas.js')}}"></script>
    <script src="{{asset('backend/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('backend/js/template.js')}}"></script>
    <script src="{{asset('backend/js/settings.js')}}"></script>
    <script src="{{asset('backend/js/todolist.js')}}"></script>

    <!-- Custom js for this page-->
    <script src="{{asset('backend/js/dashboard.js')}}"></script>
    <script src="{{asset('backend/js/Chart.roundedBarCharts.js')}}"></script>
</head>

<body>
<div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="d-flex align-items-center card-body p-3 px-3 flex-column">
                            @for($i=0;$i<$booking->pax;$i++)
                                <div class="e-voucher d-flex mb-2">
                                    <!-- 90deg rotate badge -->
                                    <div class="d-flex e-voucher-badge">
                                        <span>TECHPOLITAN</span>
                                    </div>
                                    <div class="w-75 p-4">
                                        <h4>E-VOUCHER CO-WORKING SPACE</h4>
                                        <h3>{{$booking->plan->name}}</h3>
                                        <p>#CW-{{$booking->user_id}}-{{$booking->id}}-{{$i+1}} - {{Carbon\Carbon::parse($booking->date)->format('d/m/Y')}}</p>
                                        <ul class="mb-0">
                                            <li>Open start 9am - 7pm</li>
                                            <li>Please show this e-voucher to our staff</li>
                                            <li>E-Voucher can be used to redeem a mineal water</li>
                                            <li>E-Voucher are nob refundable </li>
                                        </ul>
                                    </div>
                                    <div class="w-25 p-4 d-flex justify-content-center align-items-center" style="border-left: 1px solid;">
                                        <img src="{{asset('admin/dist/img/techpolitan-logo.jpeg')}}" height="130" class="card-img-left" alt="">
                                    </div>
                                </div>
                                @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            window.print();
        </script>
</body>


</html>