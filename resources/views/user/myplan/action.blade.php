<a href="{{route('user-payment-invoice', $model)}}" id="invoice" class="btn btn-outline-primary py-2">Invoice</a>
<button href="{{route('user-myplan-evoucher', $model)}}" id="e-voucher"  class="btn btn-outline-primary py-2 @if($model->status != 'Paid')disabled @endif" @if($model->status != 'Paid') disabled @endif >E-Voucher</button>

<script>
    $(`button#e-voucher`).on('click', function(e){
        e.preventDefault();

        var href = $(this).attr('href');

        window.location.href = href;
    })
</script>