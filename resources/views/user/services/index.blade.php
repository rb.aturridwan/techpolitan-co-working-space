@extends('user/layouts/app')

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-dark">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Services</li>
    </ol>
</nav>

<div class=" contact d-flex mb-5">
    <form action="{{url('services')}}" class="php-email-form" action="GET">
        @csrf
        <div class="align-items-end contact d-flex">
            <!-- form search schedule -->
            <div class="form-group" style="
                                margin-right: 1.5rem;
                            ">
                <label for="">Date</label>
                <input type="date" class="form-control" id="date" name="date" placeholder="Search Schedule" value="{{$date}}">
            </div>
            <div class="form-group" style="
                                margin-right: 1.5rem;
                            ">
                <label for="">Pax</label>
                <input type="number" class="form-control" id="pax" name="pax" placeholder="Pax" value="{{$pax}}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="section-title">
            <h3>Search Result for {{Carbon\Carbon::parse($date)->format('d/m/Y')}}</h3>
            <h3>Daily Pass</h3>
        </div>
    </div>
</div>

<div class="card-columns">

    @foreach($schedules as $s)

    @for($i = 0; $i < count($s); $i++) <?php
                                        $schedule = $s[$i];
                                        $plan = $s[$i]['plan'];
                                        ?> <div class="card">
@if($plan->image)
                            <img src="{{asset('images/plan')}}/{{$plan->image}}" class="img-fluid card-img-top" alt="">
                            @else
                            <img src="{{asset('images/products/daily-pass-thumb.webp')}}" class="img-fluid card-img-top" alt="">
                            @endif
        <div class="card-body">
            <h4>{{ $plan->name }}</h4>
            <p>{{ $plan->category->name}}</p>
            <p>{{ $schedule['pax']}} left</p>
            <div class="mb-3 d-flex justify-content-end">
                <div class="" style="
                                            font-weight: bold;
                                            font-size: 1.3rem;
                                        ">IDR&nbsp;{{$plan->price}}
                </div>
            </div>
        </div>
        <div class="card-footer bg-transparent d-flex justify-content-end">
            <form action="{{url('basket')}}" method="POST" class="php-email-form">
                @csrf
                <input type="hidden" name="plan_id" value="{{$plan->id}}">
                <input type="hidden" name="date" value="{{$date}}">

                <div class="align-items-lg-end d-flex justify-content-end">
                    <div class="align-items-center d-flex form-group my-0" style="margin-right: 1rem;padding: 0;">
                        <label for="">Pax</label>
                        <input type="number" min="1" class="form-control ml-2" id="pax" name="qty" placeholder="Pax" value="{{$pax}}" required>

                    </div>
                    @if(Carbon\Carbon::parse($date)->format('Y-m-d') <= Carbon\Carbon::now()->format('Y-m-d'))
                        @if(Carbon\Carbon::now() < Carbon\Carbon::today()->addHour(19) )
                        <button class="btn btn-primary @if(!$schedule['available']) disabled @endif"  @if(!$schedule['available']) disabled @endif type="submit" >@if(!$schedule['available']) Booked @else Book @endif</button>
                        @else
                        <button class="btn btn-primary disabled" type="submit" disabled>Closed</button>
                        @endif
                    @else
                        <button class="btn btn-primary @if(!$schedule['available']) disabled @endif"  @if(!$schedule['available']) disabled @endif type="submit" >@if(!$schedule['available']) Booked @else Book @endif</button>
                    @endif
                </div>
            </form>
        </div>
</div>


@endfor

@endforeach

</div>


<!-- content -->

@endsection

@push('js')

<script>
    $(document).ready(function() {
        $(document).find('#serviceMenu').addClass('active');
    });
</script>

@endpush