<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\PrivateOfficeRequest;
use App\Models\User;
use App\Models\Plan;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class BookingController extends Controller
{

    public function searchSchedule(Request $r)
    {
        // from get request
        $date = $r->get('date');
        $pax = $r->get('pax');

        $date = Carbon::parse($date)->format('Y-m-d');

        $schedule = Booking::listAvailableScheduleDaily($date, $pax);

        // return $schedule;

        return view('searchresult', [
            'title' => 'Search Schedule | Techpolitan',
            'schedules' => $schedule,
            'date' => $date,
            'pax' => $pax
        ]);
    }

    public function service(Request $r)
    {
        // from get request
        $date = $r->get('date');
        $pax = $r->get('pax');

        $date = Carbon::parse($date)->format('Y-m-d');

        $schedule = Booking::listAvailableScheduleDaily($date, $pax);

        // return $schedule;

        return view('user.services.index', [
            'title' => 'Search Services | Techpolitan',
            'schedules' => $schedule,
            'date' => $date,
            'pax' => $pax
        ]);
    }

    public function basket(Request $r)
    {


        // check auth
        if (!Auth::check()) {
            return redirect()->intended('login');
        }

        $date = $r->post('date');
        $pax = $r->post('qty');
        $plan_id = $r->post('plan_id');

        $plan = Plan::find($plan_id);
        $date = Carbon::parse($date)->format('Y-m-d');

        $price =  $plan->getRawOriginal('price');


        $subtotal = $price *  $pax;
        $tax = $subtotal * 0.1;
        $total = $subtotal + $tax;

        $schedule = [
            'date' => $date,
            'pax' => $pax,
            'plan' => $plan,
            'subtotal' => number_format($subtotal, 2, ',', '.'),
            'tax' => number_format($tax, 2, ',', '.'),
            'total' => number_format($total, 2, ',', '.'),
        ];


        return view('user.payments.basket', [
            'title' => 'Basket | Techpolitan',
            'schedule' => $schedule
        ]);
    }

    function proceedPayment(Request $r)
    {
        // check auth
        if (!Auth::check()) {
            return redirect()->intended('login');
        }

        $date = $r->post('date');
        $date = Carbon::parse($date)->format('Y-m-d');

        $pax = $r->post('qty');
        $plan_id = $r->post('plan_id');
        $plan = Plan::find($plan_id);

        $price =  $plan->getRawOriginal('price');


        $subtotal = $price * $pax;
        $tax = $subtotal * 0.1;
        $total = $subtotal + $tax;

        $booking = new Booking();
        $booking->user_id = Auth::user()->id;
        $booking->plan_id = $plan_id;
        $booking->date = $date;
        $booking->pax = $pax;
        $booking->price = $total;
        $booking->status = 'unpaid';
        // generate invoice number from datetime
        $booking->invoice_no = 'INV-' . Carbon::now()->format('YmdHis') ;
        $booking->save();

        return view('user.payments.payment', [
            'booking' => $booking,
            'title' => 'Payment | Techpolitan',
            'plan' => $plan,
        ]);

    }

    public function evoucher($id){
        $user = Auth::user();
        $booking = Booking::where('user_id', $user->id)->find($id);

        return view('user.payments.evoucher', [
            'title' => 'E-Voucher | Techpolitan',
            'booking' => $booking,
        ]);
    }

    public function printEvoucher($id){
        if (!Auth::check()) {
            return redirect()->intended('login');
        }
        $user = Auth::user();
        $booking = Booking::where('user_id', $user->id)->find($id);

        if($booking->getRawOriginal('status') != 'paid'){
            return redirect('myplans');
        }
        return view('user.payments.printevoucher', [
            'title' => 'E-Voucher | Techpolitan',
            'booking' => $booking,
        ]);
        
    }

    public function submitPrivateOffice(Request $r){

        $privsateOffice = PrivateOfficeRequest::create($r->all());

        session()->flash('message', 'Post successfully updated.');

        return redirect('/');
    }
    
}
