<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\User;
use App\Models\Plan;
use App\Models\Payment;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class PaymentController extends Controller
{

    public function payFromPaymentList(Request $r){

        $user = Auth::user();

        $booking = Booking::where('user_id', $user->id)->find($r->post('id'));


        $plan = Plan::withTrashed()->find($booking->plan_id);

        
        return view('user.payments.payment', [
            'title' => 'Payment | Techpolitan',
           'booking' => $booking,
            'user' => $user,
            'plan' => $plan
        ]);
    }
    

    public function uploadPaymentProof(Request $r){

        request()->validate([
            'payment-proof'  => 'required|mimes:pdf,jpg,jpeg,png|max:2048',
          ]);

        $user = Auth::user();

        if($r->hasFile('payment-proof')){
            $file = $r->file('payment-proof');
            $fileName = $file->getClientOriginalName();
            $file->move(public_path('/uploads/payment-proof/'.$r->post('booking_id')."/"), $fileName);

            $booking = Booking::where('user_id', $user->id)->find($r->post('booking_id'));

            // look for exist payment
            $payment = Payment::where('booking_id', $booking->id)->first();
            if($payment){
                $payment->proof = $r->post('booking_id')."/".$fileName;
                $payment->status = "waiting for confirmation";
                $payment->save();

            }else{
                $payment = new Payment;
                $payment->booking_id = $booking->id;
                $payment->user_id = $user->id;
                $payment->payment_method= "Bank Transfer";
                $payment->proof =$r->post('booking_id')."/".$fileName;
                $payment->amount = $booking->getRawOriginal('price');
                $payment->date = Carbon::now()->format('Y-m-d');
                $payment->status = "waiting for confirmation";
                $payment->save();
            }
        }

        return \response()->json([
            'status' => 'success',
            'message' => 'Payment proof uploaded successfully',
        ]);
    }

    public function invoice($id){
        $user = Auth::user();
        $booking = Booking::where('user_id', $user->id)->find($id);

        return view('user.payments.invoice', [
            'title' => 'E-Voucher | Techpolitan',
            'booking' => $booking,
        ]);
    }

    public function printInvoice($id){
        if (!Auth::check()) {
            return redirect()->intended('login');
        }
        $user = Auth::user();
        $booking = Booking::where('user_id', $user->id)->find($id);

        return view('user.payments.printinvoice', [
            'title' => 'E-Voucher | Techpolitan',
            'booking' => $booking,
        ]);
        
    }

}
