<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Plan;
use App\Models\PrivateOfficeRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{




    public function users(Request $request)
    {
        $users = User::whereHas("roles", function ($q) use ($request) {
            $q->where("name", $request->type == "admin" ? "=" : "!=", "administrator");
        })->orderBy("name", "ASC")->get();

        return datatables()->of($users)
            ->addColumn('action', 'admin.user.action')
            ->addIndexColumn()
            ->toJson();
    }

    public function booking(Request $request){
        $booking = Booking::orderBy('id', 'DESC')->get();

        return datatables()->of($booking)
            ->addColumn('action', 'admin.booking.action')
            ->addColumn('order_id', function (Booking $model) {
                return 'CW-'.$model->user_id.'-'.$model->id;
            })
            ->addColumn('plan', function (Booking $model) {
                return $model->plan->name;
            })
            ->addColumn('payment_status', function (Booking $model) {
                return @$model->payment->status;
            })
            ->addColumn('payment_proof', function (Booking $model) {
                return @$model->payment->proof;
            })
            ->addIndexColumn()
            ->toJson();
    }


    public function plan(Request $request){
        $plan = Plan::orderBy('id', 'DESC')->get();

        return datatables()->of($plan)
            ->addColumn('action', 'admin.plan.action')
            ->addColumn('category', function (Plan $model) {
                return $model->category->name;
            })
            ->addIndexColumn()
            ->toJson();
    }

    public function planTrashed(Request $request){
        $plan = Plan::onlyTrashed()->orderBy('id', 'DESC')->get();

        return datatables()->of($plan)
            ->addColumn('action', 'admin.plan.action')
            ->addColumn('category', function (Plan $model) {
                return $model->category->name;
            })
            ->addIndexColumn()
            ->toJson();
    }

    public function category(Request $request){
        $category = Category::orderBy('id', 'DESC')->get();

        return datatables()->of($category)
            ->addColumn('action', 'admin.category.action')
            ->addIndexColumn()
            ->toJson();
    }

    public function categoryTrashed(Request $request){
        $category = Category::onlyTrashed()->orderBy('id', 'DESC')->get();

        return datatables()->of($category)
            ->addColumn('action', 'admin.category.action')
            ->addIndexColumn()
            ->toJson();
    }

    public function myplan(Request $request){
        $myplan = Booking::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();


        return datatables()->of($myplan)
            ->addColumn('action', 'user.myplan.action')
            ->addColumn('order_id', function (Booking $model) {
                return 'CW-'.$model->user_id.'-'.$model->id;
            })
            ->addColumn('plan', function (Booking $model) {
                return $model->plan->name;
            })
            ->addIndexColumn()
            ->toJson();
    }

    public function payment(Request $request){
        $myplan = Booking::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();


        return datatables()->of($myplan)
            ->addColumn('action', 'user.payments.action')
            ->addIndexColumn()
            ->toJson();
    }

    public function privateOfficeRequest(Request $request){
        $privateOfficeRequest = PrivateOfficeRequest::orderBy('id', 'DESC')->get();

        return datatables()->of($privateOfficeRequest)
            ->addColumn('action', 'admin.privateofficerequest.action')
            ->addIndexColumn()
            ->toJson();
    }

}

