<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\User;
use App\Models\Plan;
use App\Models\Payment;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class BookingController extends Controller
{

    public function __construct()
    {
        // $this->middleware("permission:read booking")->only(["index"]);
        // $this->middleware("permission:create booking")->only(["create", "store"]);
        // $this->middleware("permission:update booking")->only(["edit", "update"]);
        // $this->middleware("permission:delete booking")->only(["destroy"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.booking.index', [
            'title' => 'Booking | Techpolitan'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.booking.create', [
            'title' => 'Tambah Booking | Techpolitan',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $this->validate($request, [
            'name' => 'required|min:5',
            'gender' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'position_id' => 'required',
            'group_id' => 'required',
            'department_id' => 'required',
            'status' => 'required',
            'salary' => 'required',
            'norek' => 'required'
        ]);

        try {
            DB::beginTransaction();
            User::Find($booking->user->id)->update([
                "email" => $request->email,
                "name" => $request->name,
            ]);

            $data = [
                'name' => $request->name,
                'gender' => $request->gender,
                'address' => $request->address,
                'phone' => $request->phone,
                'email' => $request->email,
                'position_id' => $request->position_id,
                'group_id' => $request->group_id,
                'department_id' => $request->department_id,
                'status' => $request->status,
                'salary' => $request->salary,
                'norek' => $request->norek
            ];

            $booking->update($data);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()->route('admin.booking.index')->with('danger', 'Data Gagal Diupdate' . $e->getMessage());
        }

        return redirect()->route('admin.booking.index')->with('info', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();

        return redirect()->route('admin.booking.index')->with('danger', 'Data Berhasil Dihapus');
    }

    public function confirm(Request $r)
    {
        $booking = Booking::find($r->id);
        $user = User::find($booking->user_id); 
        $booking->update([
            'status' => 'Paid'
        ]);

        $payment = $booking->payment;

        if($payment) {
            $payment->update([
                'status' => 'Confirmed'
            ]);
        }else{
            $payment = new Payment;
            $payment->booking_id = $booking->id;
            $payment->user_id = $user->id;
            $payment->payment_method= "Bank Transfer";
            $payment->amount = $booking->getRawOriginal('price');
            $payment->date = Carbon::now()->format('Y-m-d');
            $payment->status = "Confirmed";
            $payment->save();
        }

        $payment->update([
            'status' => 'Confirmed'
        ]);

        return redirect()->route('admin.booking.index')->with('info', 'Data Berhasil Diupdate');
    }

    public function printInvoiceAdmin($id){
        if (!Auth::check()) {
            return redirect()->intended('login');
        }
        $booking = Booking::where('id', $id)->first();

        return view('user.payments.printinvoice', [
            'title' => 'E-Voucher | Techpolitan',
            'booking' => $booking,
        ]);
        
    }

    // print all booking
    public function printAllBooking(){
        if (!Auth::check()) {
            return redirect()->intended('login');
        }
        $bookings = Booking::all();

        return view('admin.booking.printbooking', [
            'title' => 'All Booking | Techpolitan',
            'bookings' => $bookings,
        ]);
    }
}
