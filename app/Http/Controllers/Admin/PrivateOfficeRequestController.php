<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Category;
use App\Models\PrivateOfficeRequest;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class PrivateOfficeRequestController extends Controller
{

    public function __construct()
    {
        // $this->middleware("permission:read booking")->only(["index"]);
        // $this->middleware("permission:create booking")->only(["create", "store"]);
        // $this->middleware("permission:update booking")->only(["edit", "update"]);
        // $this->middleware("permission:delete booking")->only(["destroy"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.privateofficerequest.index', [
            'title' => 'Private Office | Techpolitan',
            'isRecycleBin' => false,
        ]);
    }

    public function indexTrashed()
    {
        return view('admin.privateofficerequest.index', [
            'title' => 'Private Office | Techpolitan',
            'isRecycleBin' => true,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PrivateOfficeRequest $p)
    {

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrivateOfficeRequest $p, $id)
    {

        $privateofficerequest = PrivateOfficeRequest::find($id);
        $privateofficerequest->delete();

        return redirect()->route('admin.private-office-request.index')->with('danger', 'Data Berhasil Dihapus');
    }

}
