<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Category;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function __construct()
    {
        // $this->middleware("permission:read booking")->only(["index"]);
        // $this->middleware("permission:create booking")->only(["create", "store"]);
        // $this->middleware("permission:update booking")->only(["edit", "update"]);
        // $this->middleware("permission:delete booking")->only(["destroy"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.index', [
            'title' => 'Category | Techpolitan',
            'isRecycleBin' => false,
        ]);
    }

    public function indexTrashed()
    {
        return view('admin.category.index', [
            'title' => 'Category | Techpolitan',
            'isRecycleBin' => true,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create', [
            'title' => 'Tambah Booking | Techpolitan',
            'categories' => Category::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'name' => 'required|unique:categories',
            'description' => '',
        ]);

        $plan = new Category;
        $plan->name = $request->name;
        $plan->description = $request->description;
        $plan->save();

        return redirect()->route('admin.category.index')->with('success', 'Data berhasil ditambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {

        return view('admin.category.edit', [
            'title' => 'Edit Booking | Techpolitan',
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => '',
        ]);

        try {
            DB::beginTransaction();

            $plan = Category::find($request->id);
            $plan->name = $request->name;

            $plan->description = $request->description;

            $plan->update();

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()->route('admin.category.index')->with('danger', 'Data Gagal Diupdate' . $e->getMessage());
        }

        return redirect()->route('admin.category.index')->with('info', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.category.index')->with('danger', 'Data Berhasil Dihapus');
    }

    public function restore($id)
    {
        $category = Category::withTrashed()->findOrFail($id);
        $category->restore();

        return redirect()->route('admin.category.index')->with('info', 'Data Berhasil Dikembalikan');
    }
}
