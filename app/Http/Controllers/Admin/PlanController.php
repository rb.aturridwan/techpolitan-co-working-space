<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Plan;
use Auth;
use Carbon\Carbon;
use Error;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class PlanController extends Controller
{

    public function __construct()
    {
        // $this->middleware("permission:read booking")->only(["index"]);
        // $this->middleware("permission:create booking")->only(["create", "store"]);
        // $this->middleware("permission:update booking")->only(["edit", "update"]);
        // $this->middleware("permission:delete booking")->only(["destroy"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.plan.index', [
            'title' => 'Plan | Techpolitan',
            'isRecycleBin' => false,
        ]);
    }

    public function indexTrashed()
    {
        return view('admin.plan.index', [
            'title' => 'Plan | Techpolitan',
            'isRecycleBin' => true,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plan.create', [
            'title' => 'Tambah Booking | Techpolitan',
            'categories' => Category::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => '',
            'pax' => 'required',
            'category_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048',
        ]);

        $plan = new Plan;

        if($request->hasFile('image')) {

            $image = $request->file('image');
            $plan->slug = Str::slug($request->name, '-');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/plan/'), $imageName);
            $plan->name = $request->name;
            $plan->price = $request->price;
            $plan->image = $imageName;
            $plan->description = $request->description;
            $plan->pax = $request->pax;
            $plan->category_id = $request->category_id;
            $plan->save();

            return redirect()->route('admin.plan.index')->with('success', 'Data berhasil ditambahkan');
        }
        
        return redirect()->back()->with('danger', 'Data Gagal Ditambahkan')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plan = Plan::findOrFail($id);
        return view('admin.plan.show', [
            'title' => 'Detail Plan | Techpolitan',
            'plan' => $plan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Plan $plan)
    {

        return view('admin.plan.edit', [
            'title' => 'Edit Booking | Techpolitan',
            'plan' => $plan,
            'categories' => Category::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'description' => '',
            'pax' => 'required',
            'category_id' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max:2048',

        ]);

        try {
            DB::beginTransaction();

            $plan = Plan::find($request->id);
            $plan->name = $request->name;
            $plan->slug = Str::slug($request->name, '-');
            $plan->price = $request->price;
            $plan->description = $request->description;
            $plan->pax = $request->pax;
            $plan->category_id = $request->category_id;

            if($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('images/plan/'), $imageName);
                $plan->image = $imageName;
            }

            $plan->update();


            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()->route('admin.plan.index')->with('danger', 'Data Gagal Diupdate' . $e->getMessage());
        }

        return redirect()->route('admin.plan.index')->with('info', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan)
    {
        $plan->delete();

        return redirect()->route('admin.plan.index')->with('danger', 'Data Berhasil Dihapus');
    }

    public function restore($id)
    {
        $plan = Plan::withTrashed()->findOrFail($id);
        $plan->restore();

        return redirect()->route('admin.plan.index')->with('info', 'Data Berhasil Dikembalikan');
    }
}
