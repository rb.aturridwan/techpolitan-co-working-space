<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $guarded = [];

    protected $filled = [
        'user_id',
        'plan_id',
        'status',
        'date',
        'pax',
        'price',
        'payment_id',
        'invoice_no',
    ];


    // list available schedule daily on empty booking
    public static function listAvailableScheduleDaily($date, $pax = 1)
    {
        $schedule = [];

        if(Carbon::parse($date) < Carbon::today()) {
            return $schedule;
        }

        $booking = Booking::where('date', date('Y-m-d', strtotime($date)))
            ->get();
        

        // loop each day daily
        

            $plans = Plan::withoutTrashed()->get();
            $day = date('d-m-Y', strtotime($date));
            $schedule[$day] = [];
            $j = 0;

            foreach ($plans as $plan) {

                $pax_per_day = $plan->pax;
                $pax_available = $pax_per_day;
                
                $schedule[$day][$j] = [];
                $schedule[$day][$j]['plan'] = $plan;
                $schedule[$day][$j]['available'] = true;

                $i = 0;
                foreach ($booking as $b) {
                    
                    if ($b->date == $day && $b->plan_id == $plan->id) {
                        $pax_available = $pax_available - $b->pax;
                    }
                    $i++;
                }

                $schedule[$day][$j]['pax'] = $pax_available;
                if($pax_available < 1){
                    $schedule[$day][$j]['available'] = false;
                }

                $j++;
            }



        return $schedule;
        
    }

    // get list booking
    public static function getList()
    {
        return self::orderBy('created_at', 'desc')->get();
    }

    // create booking
    public static function createBooking($data)
    {
        $booking = new Booking;
        $booking->user_id = $data['user_id'];
        $booking->plan_id = $data['plan_id'];
        $booking->date = $data['date'];
        $booking->pax = $data['pax'];
        $booking->price = $data['price'];
        $booking->payment_method = $data['payment_method'];
        $booking->payment_status = $data['payment_status'];
        $booking->payment_id = $data['payment_id'];
        $booking->payment_amount = $data['payment_amount'];
        $booking->save();

        return $booking;
    }

    // update booking
    public static function updateBooking($data)
    {
        $booking = Booking::find($data['id']);
        $booking->user_id = $data['user_id'];
        $booking->plan_id = $data['plan_id'];
        $booking->date = $data['date'];
        $booking->pax = $data['pax'];
        $booking->price = $data['price'];
        $booking->payment_method = $data['payment_method'];
        $booking->payment_status = $data['payment_status'];
        $booking->payment_id = $data['payment_id'];
        $booking->payment_amount = $data['payment_amount'];
        $booking->save();

        return $booking;
    }

    // delete booking
    public static function deleteBooking($id)
    {
        $booking = Booking::find($id);
        $booking->delete();

        return $booking;
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class)->withTrashed();
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'booking_id', 'id');
    }


    // get status payment
    public function getStatusPaymentAttribute()
    {
        if ($this->payment) {
            return $this->payment->status;
        } else {
            return '-';
        }
    }

    // check is payment paid
    public function getIsPaidAttribute()
    {
        if ($this->payment) {
            return $this->payment->status == 'paid';
        } else {
            return false;
        }
    }

    public function getStatusAttribute($value)
    {
        return ucwords($value);
    }


    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = strtolower($value);
    }

    public function getDateAttribute($value)
    {
        return date('d-m-Y', strtotime($value));
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = date('Y-m-d', strtotime($value));
    }

    public function getPaxAttribute($value)
    {
        return number_format($value);
    }

    public function setPaxAttribute($value)
    {
        $this->attributes['pax'] = str_replace(',', '', $value);
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(',', '', $value);
    }

    public function getPaymentMethodAttribute($value)
    {
        return ucwords($value);
    }

    public function setPaymentMethodAttribute($value)
    {
        $this->attributes['payment_method'] = strtolower($value);
    }

    public function getPaymentStatusAttribute($value)
    {
        return ucwords($value);
    }

    public function setPaymentStatusAttribute($value)
    {
        $this->attributes['payment_status'] = strtolower($value);
    }

    public function getPaymentIdAttribute($value)
    {
        return $value;
    }

    public function setPaymentIdAttribute($value)
    {
        $this->attributes['payment_id'] = $value;
    }

    public function getPaymentAmountAttribute($value)
    {
        return number_format($value, 2);
    }

    // check if booking reach limit per day each plan
    public static function checkBookingReachLimit($plan_id, $date)
    {
        $booking = Booking::where('plan_id', $plan_id)
            ->where('date', $date)
            ->count();

        $plan = Plan::find($plan_id);

        if ($booking >= $plan->pax) {
            return false;
        } else {
            return true;
        }
    }

    // list booking per plan per date
    public static function listBookingPerPlan($plan_id, $date)
    {
        $booking = Booking::where('plan_id', $plan_id)
            ->where('date', $date)
            ->get();

        return $booking;
    }

    // list booking per user per date
    public static function listBookingPerUser($user_id, $date)
    {
        $booking = Booking::where('user_id', $user_id)
            ->where('date', $date)
            ->get();

        return $booking;
    }

    //  scope filter where not reach total pax
    public function scopeNotReachTotalPax($query, $plan_id, $date)
    {
        $booking = Booking::where('plan_id', $plan_id)
            ->where('date', $date)
            ->count();

        $plan = Plan::find($plan_id);

        if ($booking >= $plan->pax) {
            return $query->where('plan_id', $plan_id)
                ->where('date', $date)
                ->where('status', 'pending');
        } else {
            return $query->where('plan_id', $plan_id)
                ->where('date', $date)
                ->where('status', 'pending');
        }

        
    }

    // list available schedule per plan daily when not reach total pax
    public static function listAvailableSchedulePerPlan($plan_id, $date)
    {
        $booking = Booking::where('plan_id', $plan_id)
            ->where('date', $date)
            ->count();

        $plan = Plan::find($plan_id);

        $schedule = [];

        if ($booking >= $plan->pax) {
            return [
                'status' => 'reach_total_pax',
                'plan' => $plan,
                'pax_left' => $plan->pax - $booking  < 0 ? 0 : $plan->pax - $booking,
                'date' => $date,
            ];
        } else {
            return [
                'status' => 'available',
                'plan' => $plan,
                'pax_left' => $plan->pax - $booking  < 0 ? 0 : $plan->pax - $booking,
                'date' => $date,
            ];
        }
           
    }



    

}
