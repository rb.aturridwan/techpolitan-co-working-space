<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivateOfficeRequest extends Model
{
    //
    protected $guarded = [];

    protected $filled = [
        'name',
        'phone',
        'company_name',
        'capacity',
        'start_date',
        'message'
    ];


    // get list private office request
    public static function getList()
    {
        return self::orderBy('created_at', 'desc')->get();
    }

    // create private office request
    public function createPrivateOffice()
    {
        $privateOffice = new PrivateOfficeRequest();
        $privateOffice->user_id = $this->user_id;
        $privateOffice->name = $this->name;
        $privateOffice->phone = $this->phone;
        $privateOffice->company_name = $this->company_name;
        $privateOffice->capacity = $this->capacity;
        $privateOffice->start_date = $this->start_date;
        $privateOffice->message = $this->message;
        $privateOffice->save();

        return $privateOffice;
    }

    // update private office request
    public function updatePrivateOffice()
    {
        $privateOffice = PrivateOfficeRequest::find($this->id);
        $privateOffice->user_id = $this->user_id;
        $privateOffice->name = $this->name;
        $privateOffice->phone = $this->phone;
        $privateOffice->company_name = $this->company_name;
        $privateOffice->capacity = $this->capacity;
        $privateOffice->start_date = $this->start_date;
        $privateOffice->message = $this->message;
        $privateOffice->save();

        return $privateOffice;
    }

    // delete private office request

    public function deletePrivateOffice()
    {
        $privateOffice = PrivateOfficeRequest::find($this->id);
        $privateOffice->delete();

        return $privateOffice;
    }





}
