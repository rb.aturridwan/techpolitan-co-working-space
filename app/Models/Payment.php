<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //

    protected $guarded = [];

    protected $filled = [
        'user_id',
        'amount',
        'booking_id',
        'payment_method',
        'proof',
        'bank_name',
        'bank_account_name',
        'bank_account_number',
        'bank_branch',
        'date',
        'status',
        'payment_id',
    ];
    

    // get list payment

    public static function getList()
    {
        return self::orderBy('created_at', 'desc')->get();
    }

    // create payment
    public function createPayment()
    {
        $payment = new Payment();
        $payment->user_id = $this->user_id;
        $payment->amount = $this->amount;
        $payment->payment_method = $this->payment_method;
        $payment->proof = $this->proof;
        $payment->bank_name = $this->bank_name;
        $payment->bank_account_name = $this->bank_account_name;
        $payment->bank_account_number = $this->bank_account_number;
        $payment->bank_branch = $this->bank_branch;
        $payment->date = $this->date;
        $payment->status = $this->status;
        $payment->payment_id = $this->payment_id;
        $payment->save();

        return $payment;
    }

    // update payment

    

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // belongs to booking
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }

    



}
