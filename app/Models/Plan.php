<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Plan extends Model
{
    //
    use SoftDeletes;

    protected $date = ['deleted_at'];

    protected $guarded = [];

    protected $filled = [
        'name',
        'price',
        'slug',
        'description',
        'category_id',
    ];

    // get list plan
    public static function getList()
    {
        return self::orderBy('created_at', 'desc')->get();
    }

    // create plan
    public function createPlan()
    {
        $plan = new Plan();
        $plan->name = $this->name;
        $plan->price = $this->price;
        $plan->description = $this->description;
        $plan->category_id = $this->category_id;
        $plan->save();

        return $plan;
    }

    // update plan

    public function updatePlan()
    {
        $plan = Plan::find($this->id);
        $plan->name = $this->name;
        $plan->price = $this->price;
        $plan->description = $this->description;
        $plan->category_id = $this->category_id;
        $plan->save();

        return $plan;
    }

    // delete plan
    public function deletePlan()
    {
        $plan = Plan::find($this->id);
        $plan->delete();

        return $plan;
    }


    public function category()
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(',', '', $value);
    }

    public function getDescriptionAttribute($value)
    {
        return nl2br($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = str_replace("\n", '', $value);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }


    public function setSlugAttribute($value) {

        if (static::whereSlug($slug = Str::slug($value))->exists()) {
    
            $slug = $this->incrementSlug($slug);
        }
    
        $this->attributes['slug'] = $slug;
    }

    protected function incrementSlug($slug)
    {
        $count = static::whereSlug($slug)->count();
        return "{$slug}-{$count}";
    }



}