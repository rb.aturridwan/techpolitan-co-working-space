<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $date = ['deleted_at'];

    protected $guarded = [];

    protected $filled = [
        'name',
        'description',
    ];

    public function plans()
    {
        return $this->hasMany(Plan::class);
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    public function getDescriptionAttribute($value)
    {
        return nl2br($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = str_replace("\n", '', $value);
    }

    public function getPlansCountAttribute()
    {
        return $this->plans->count();
    }

    public function getPlansCountTextAttribute()
    {
        return $this->plans_count == 1 ? '1 plan' : $this->plans_count . ' plans';
    }

    public function getPlansCountTextPluralAttribute()
    {
        return $this->plans_count == 1 ? '1 plan' : $this->plans_count . ' plans';
    }
    
}
