<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/search-coworking',  'User\BookingController@searchSchedule');

Route::post('/private-office-message',  'User\BookingController@submitPrivateOffice')->name('private-office-message');

Route::middleware('web', 'auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('user-dashboard');

    Route::get('/myplans', function () {
        return view('user.myplan.index');
    })->name('user-myplan');

    Route::get('/myplans/e-voucher/{id}','User\BookingController@evoucher')->name('user-myplan-evoucher');
    Route::get('/print/e-voucher/{id}','User\BookingController@printEvoucher')->name('user-print-evoucher');

    Route::get('/payments', function () {
        return view('user.payments.index');
    })->name('user-payment');

    Route::get('/payments/invoice/{id}','User\PaymentController@invoice')->name('user-payment-invoice');
    Route::get('/print/invoice/{id}','User\PaymentController@printInvoice')->name('user-print-invoice');

    Route::post('/pay', 'User\PaymentController@payFromPaymentList')->name('user-pay');
    Route::post('/upload-payment-proof', 'User\PaymentController@uploadPaymentProof')->name('user-upload-payment-proof');

    Route::get('/services', 'User\BookingController@service')->name('user-services');

});

Route::get('/basket', function(){
    // check auth
    if(!Auth::check()){
        return redirect()->intended('login');
    }
    
    return redirect()->route('user-services');
})->name('user-basket');

Route::post('/basket', 'User\BookingController@basket')->name('user-basket');

Route::get('/proceedpayment', function(){
    // check auth
    if(!Auth::check()){
        return redirect()->intended('login');
    }
    
    return redirect()->route('user-services');
})->name('user-proceedpayment');

Route::post('/proceedpayment', 'User\BookingController@proceedPayment')->name('user-proceedpayment');





Auth::routes(['verify' => true]);
