<?php

Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/department/data', 'DataController@departments')->name('department.data');

Route::get('/booking/data', 'DataController@booking')->name('booking.data');
Route::resource('booking', 'BookingController');
Route::post('booking/{id}', 'BookingController@confirm')->name('booking.confirm');
Route::get('/print/bookings','BookingController@printAllBooking')->name('booking.printall');

Route::get('/private-office-request/data', 'DataController@privateOfficeRequest')->name('private-office-request.data');
Route::resource('private-office-request', 'PrivateOfficeRequestController');

Route::get('/category/data', 'DataController@category')->name('category.data');
Route::get('/category/dataTrashed', 'DataController@categoryTrashed')->name('category.dataTrashed');
Route::resource('category', 'CategoryController');
Route::get('category-trashed', 'CategoryController@indexTrashed')->name('category.trashed');
Route::get('category-trashed-restore/{id}', 'CategoryController@restore')->name('category.restore');


Route::get('/plan/data', 'DataController@plan')->name('plan.data');
Route::get('/plan/dataTrashed', 'DataController@planTrashed')->name('plan.dataTrashed');
Route::get('/myplan/data', 'DataController@myplan')->name('myplan.data');
Route::resource('plan', 'PlanController');
Route::get('plan-trashed', 'PlanController@indexTrashed')->name('plan.trashed');
Route::get('plan-trashed-restore/{id}', 'PlanController@restore')->name('plan.restore');

Route::get('/payment/data', 'DataController@payment')->name('payment.data');

Route::get('/user/data', 'DataController@users')->name('user.data');
Route::resource('user', 'UserController');
Route::get('/useremployee', 'UserController@indexEmployee')->name('user.employee');
Route::get('/print/invoice/{id}','BookingController@printInvoiceAdmin')->name('booking.invoice');
